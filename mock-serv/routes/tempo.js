const express = require('express');
const router = express.Router();
var faker = require('faker');
/* GET users listing. */
router.get('/', (req, res) => {
  res.send([{
		"name": faker.lorem.word(),
		"id": "1"
	},
	{
		"name": faker.lorem.word(),
		"id": "2"
	},
	{
		"name": faker.lorem.word(),
		"id": "3"
	},
	{
		"name": faker.lorem.word(),
		"id": "4"
	}
]);
});
router.get('/:tempoId', (req, res) => {
  res.send({
		"name": faker.lorem.word(),
		"id": req.params.tempoId
	});
});
module.exports = router;
