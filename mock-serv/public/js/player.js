var audio=document.getElementsByClassName("aud")[0];
var pl=document.getElementsByClassName("play")[0].firstElementChild;
var grow=document.getElementsByClassName("grow")[0];
var tim=document.getElementsByClassName("timer")[0];
var vol=document.getElementsByClassName("vol-range")[0];
var timer;
var dur=0;
var minutes=0;
var seconds=0;
var ratio;
audio.onloadedmetadata = function() {
dur=parseInt(audio.duration);
minutes = parseInt(audio.duration / 60, 10);
seconds = parseInt(audio.duration % 60);
ratio = 300/audio.duration;
};
var i=0;
var j=0;
var counter=0;
function play(){
	if(pl.className=="fas fa-pause-circle"){
	audio.pause();
	pl.className="fas fa-play-circle";
	clearTimeout(timer);
	}
	else {
	audio.play();
	pl.className="fas fa-pause-circle";
	timer=setTimeout(function recursion(){
		min = parseInt(audio.currentTime / 60, 10);
		sec = parseInt(audio.currentTime % 60);
		counter++;
		if (seconds>sec)
			if((seconds-sec)<10)
				tim.innerHTML=(minutes-min)+":0"+(seconds-sec);
			else tim.innerHTML=(minutes-min)+":"+(seconds-sec);
		else
			tim.innerHTML=(minutes-min-1)+":"+(seconds-sec+59);
		if (audio.duration-audio.currentTime){ 
				grow.style.width=counter*ratio+'px';
				timer=setTimeout(recursion,1000);
		}
		else{ tim.innerHTML="0:00";counter=0; grow.style.width=0+'px'; play();}
},1000)
	}
}
	var warp=document.getElementsByClassName("vol-warp")[0];
    var wid=warp.offsetWidth;
	var gr=document.getElementsByClassName("grow-y")[0];
var thumbElem = warp.children[0].children[1];
    thumbElem.onmousedown = function(e) {
      var thumbCoords = getCoords(thumbElem);
      var shiftX = e.pageX - thumbCoords.left;
      // shiftY здесь не нужен, слайдер двигается только по горизонтали

      var sliderCoords = getCoords(warp);

      document.onmousemove = function(e) {
        //  вычесть координату родителя, т.к. position: relative
        var newLeft = e.pageX - shiftX - sliderCoords.left;

        // курсор ушёл вне слайдера
        if (newLeft < -(warp.offsetWidth - thumbElem.offsetWidth)/2) {
          newLeft = -(warp.offsetWidth - thumbElem.offsetWidth)/2;
        }
        var rightEdge = (warp.offsetWidth - thumbElem.offsetWidth);
        if (newLeft > rightEdge) {
          newLeft = rightEdge;
        }

		gr.style.width=newLeft+'px';
		if(newLeft>1)audio.volume=newLeft/44;
		else audio.volume=0;
		
      }
      document.onmouseup = function() {
        document.onmousemove = document.onmouseup = null;
      };

      return false; // disable selection start (cursor change)
    };

    thumbElem.ondragstart = function() {
      return false;
    };

    function getCoords(elem) { // кроме IE8-
      var box = elem.getBoundingClientRect();
      return {
		 top: box.top + pageYOffset,
        left: box.left + pageXOffset
      };

    }
    
var flag=0;
function GenMood(){
if (flag) {genre(); mood();
var border=document.getElementsByClassName("bar")[0];
border.firstElementChild.style.borderBottom="3px solid #E70C2F";
border.lastElementChild.style.borderBottom="none";
}
}
function InstrTemp(){
if (!flag) {Instr(); Tempo();
var border=document.getElementsByClassName("bar")[0];
border.lastElementChild.style.borderBottom="3px solid #E70C2F";
border.firstElementChild.style.borderBottom="none";
}
}
var k;
function genre(){
		flag=0;
		var genr=document.getElementsByClassName("genre")[0];
		var gena=document.getElementsByClassName("gena")[0];
		gena.innerText="Genre";
		if(genr.firstElementChild)genr.firstElementChild.remove();
		var div1=document.createElement('div');
		div1.className="to-center";
		genr.appendChild(div1);
	for(k=0;k<gen.length;k++){
		var div=document.createElement('div');
		var i=document.createElement('i');
		div1.appendChild(div);
		div.className="add";
		div.innerHTML=gen[k].name;
		div.appendChild(i);
		div.style.width="80%";
		i.className="fas fa-plus";
		i.onclick=function addGenre(event){
	var res=document.getElementsByClassName("reslt-genre")[0];
	var cl=event.target.parentElement.cloneNode(true);
	cl.lastElementChild.className="fas fa-minus";
	cl.lastElementChild.onclick=function remove(event){
		event.target.parentElement.remove();
	}
	if (res.childNodes[1]) res.replaceChild(cl,res.lastElementChild);
	else res.appendChild(cl);
	
}
	}
}
function mood(){
	var moda=document.getElementsByClassName("moda")[0];
	moda.innerText="Mood";
	var moo=document.getElementsByClassName("mood")[0];
	if(moo.firstElementChild)moo.firstElementChild.remove();
	var div1=document.createElement('div');
	moo.appendChild(div1);
	div1.className="to-center";
for(var k=0;k<md.length;k++){
	var div=document.createElement('div');
	var i=document.createElement('i');
	div1.appendChild(div);
	div.className="add";
	div.innerHTML=md[k].name;
	div.appendChild(i);
	div.style.width="80%";
	i.className="fas fa-plus";
	i.onclick=function addMood(event){
	var res=document.getElementsByClassName("reslt-mood")[0];
	var cl=event.target.parentElement.cloneNode(true);
	cl.lastElementChild.className="fas fa-minus";
	cl.lastElementChild.onclick=function remove(event){
		event.target.parentElement.remove();
	}
	if (res.childNodes[1]) res.replaceChild(cl,res.lastElementChild);
	else res.appendChild(cl);
	
}
}
}
function Instr(){
	var ins=document.getElementsByClassName("gena")[0];
	ins.innerText="Instruments";
	flag=1;
	var ins=document.getElementsByClassName("instrum")[0];
	ins.firstElementChild.remove();
	var div1=document.createElement('div');
	ins.appendChild(div1);
	div1.className="to-center";
for(var k=0;k<instum.length;k++){
	var div=document.createElement('div');
	var i=document.createElement('i');
	div1.appendChild(div);
	div.className="add";
	div.innerHTML=instum[k].name;
	div.appendChild(i);
	div.style.width="80%";
	i.className="fas fa-plus";
	i.onclick=function addMood(event){
	var res=document.getElementsByClassName("reslt-instr")[0];
	var cl=event.target.parentElement.cloneNode(true);
	cl.lastElementChild.className="fas fa-minus";
	cl.lastElementChild.onclick=function remove(event){
		event.target.parentElement.remove();
	}
	if (res.childNodes[1]) res.replaceChild(cl,res.lastElementChild);
	else res.appendChild(cl);
	
}
}
}
function Tempo(){
	var te=document.getElementsByClassName("moda")[0];
	te.innerText="Tempo";
	var tem=document.getElementsByClassName("tempo")[0];
	tem.firstElementChild.remove();
	var div1=document.createElement('div');
	tem.appendChild(div1);
	div1.className="to-center";
for(var k=0;k<tempo.length;k++){
	var div=document.createElement('div');
	var i=document.createElement('i');
	div1.appendChild(div);
	div.className="add";
	div.innerHTML=tempo[k].name;
	div.appendChild(i);
	div.style.width="80%";
	i.className="fas fa-plus";
	i.onclick=function addMood(event){
	var res=document.getElementsByClassName("reslt-tempo")[0];
	var cl=event.target.parentElement.cloneNode(true);
	cl.lastElementChild.className="fas fa-minus";
	cl.lastElementChild.onclick=function remove(event){
		event.target.parentElement.remove();
	}
	if (res.childNodes[1]) res.replaceChild(cl,res.lastElementChild);
	else res.appendChild(cl);
	
}
}
}
var mot=document.getElementsByClassName("road-warp")[0];
mot.addEventListener("click",function(e){
if(pl.className=="fas fa-play-circle") play();
if (e.target.className!="motion"){
	var x = e.offsetX;
	play();
	counter=x/ratio;
	play();
	grow.style.width=x+'px';
	audio.currentTime=parseInt(x/300*dur);
			
}
});
mot.addEventListener("mousemove",function (e){
var show=document.getElementsByClassName("sh-time")[0];
show.style.left=e.clientX-show.offsetWidth/2+'px';
if(parseInt(e.offsetX/ratio %60)>9)
	show.innerHTML=parseInt(e.offsetX/ratio/60)+":"+parseInt(e.offsetX/ratio %60);
else 	show.innerHTML=parseInt(e.offsetX/ratio/60)+":0"+parseInt(e.offsetX/ratio %60);
show.style.display="block";
mot.onmouseout=function(){show.style.display="none";};
});
var list=document.getElementsByClassName("mus-list")[0];
var curImg=document.getElementsByClassName("act-img")[0];
var curName=document.getElementsByClassName("music-name")[0];
var Cont=document.createElement('div');
Cont.className="cont";
var musCont=document.createElement('div');
musCont.className="lister";
var imgCont=document.createElement('div');
imgCont.className="poster-listed";
var textCont=document.createElement('div');
textCont.className="text-listed";
var iCont=document.createElement('div');
iCont.className="mus-add";
var save=document.createElement('i');
save.className="fas fa-plus";
var id=document.createElement('div');
id.style.display="none";
iCont.appendChild(save);
iCont.appendChild(id);
var audioCont=document.createElement('audio');
Cont.appendChild(musCont);
Cont.appendChild(iCont);
musCont.appendChild(imgCont);
musCont.appendChild(textCont);
musCont.appendChild(audioCont);
function load(){ 
	var my=document.getElementsByClassName("my")[0];
	var al=document.getElementsByClassName("all")[0];
	al.style.borderBottom="3px solid #E70C2F";	
	my.style.borderBottom="none";
while (list.firstChild) {
		list.removeChild(list.firstChild);
		}
for(var p=0;p<music.length;p++){
	var lister=Cont.cloneNode(true);
	lister.children[0].children[0].style.backgroundImage="url("+music[p].img+")";
	lister.children[0].children[1].innerHTML=music[p].name+" - "+music[p].group;
	lister.children[1].children[1].innerHTML=music[p].id;
	lister.children[1].onclick=function(e){
		userInf[0].musId[userInf[0].musId.length]=e.currentTarget.lastElementChild.innerHTML;
	};
	lister.children[0].children[2].innerHTML=music[p].src;
	list.appendChild(lister);
lister.firstElementChild.onclick=function(e){
curImg.style.backgroundImage=e.currentTarget.children[0].style.backgroundImage;
curName.innerHTML=e.currentTarget.children[1].innerHTML;
audio.src=e.currentTarget.children[2].innerHTML;
if(pl.className=="fas fa-pause-circle"){
	pl.className="fas fa-play-circle";
}
clearTimeout(timer);
counter=0;
audio.curruntTime=0;
grow.style.width=0;
};
};
};
function loadMy(){ 
	var my=document.getElementsByClassName("my")[0];
	var al=document.getElementsByClassName("all")[0];
	my.style.borderBottom="3px solid #E70C2F";	
	al.style.borderBottom="none";
	while (list.firstChild) {
		list.removeChild(list.firstChild);
		}
for(var p=0;p<userInf[0].musId.length;p++){
	for(var i=0;i<music.length;i++){
		if(music[i].id==userInf[0].musId[p]){
	var lister=Cont.cloneNode(true);
	lister.children[0].children[0].style.backgroundImage="url("+music[i].img+")";
	lister.children[0].children[1].innerHTML=music[i].name+" - "+music[i].group;
	lister.children[1].children[1].innerHTML=music[i].id;
	/*lister.children[1].onclick=function(e){
		userInf[0].musId[userInf[0].musId.length]=e.currentTarget.lastElementChild.innerHTML;
	};*/
	lister.children[0].children[2].innerHTML=music[i].src;
	list.appendChild(lister);
lister.firstElementChild.onclick=function(e){
curImg.style.backgroundImage=e.currentTarget.children[0].style.backgroundImage;
curName.innerHTML=e.currentTarget.children[1].innerHTML;
audio.src=e.currentTarget.children[2].innerHTML;
if(pl.className=="fas fa-pause-circle"){
	pl.className="fas fa-play-circle";
}
clearTimeout(timer);
counter=0;
audio.curruntTime=0;
grow.style.width=0;
};
};
}
};
};
var srchPar="By name";
function OpenFinal(){
	var block=document.getElementsByClassName("final")[0];
	if (block.style.display=="flex")
		block.style.display="none";
	else{
		block.style.display="flex";
}
}
function Drop(){
	var params=document.getElementsByClassName("srch-params")[0];
	if (params.style.display=="block")
		params.style.display="none";
	else{
		params.style.display="block";
		params.onclick=function(el){
			srchPar=el.target.innerHTML;
		for(var i=0; i<params.children.length;i++)
			if(params.children[i].style.backgroundColor!="#f9f9f9"){
				params.children[i].style.backgroundColor="#f9f9f9"
				params.children[i].style.color="#000";
			}
			el.target.style.backgroundColor="#E70C2F";
			el.target.style.color="#fff";
		}
	}
}

var srch=document.getElementsByClassName("srch")[0];
var srchBtn=document.getElementsByClassName("search-btn")[0];
srch.addEventListener("input", function(event) {
        srchBtn.click();
});

function Search(){
	if(srch.value.length){
		while (list.firstChild) {
		list.removeChild(list.firstChild);
		}
		var qw=0;
		switch(srchPar){
		case "By name":
			for(var j=0;j<music.length;j++, qw=0){
				for(var k=0;k<srch.value.length & k<music[j].name.length;k++)
					if(srch.value[k].toLowerCase()==music[j].name[k].toLowerCase()) qw+=1;
				if (qw>srch.value.length-1) {
					lister=Cont.cloneNode(true);
					lister.children[0].children[0].style.backgroundImage="url("+music[j].img+")";
					lister.children[0].children[1].innerHTML=music[j].name+" - "+music[j].group;
					lister.children[0].children[2].innerHTML=music[j].src;
					lister.children[1].children[1].innerHTML=music[j].id;
					lister.children[1].onclick=function(e){
						userInf[0].musId[userInf[0].musId.length]=e.currentTarget.lastElementChild.innerHTML;
					};
					lister.onclick=lister.onclick=function (e){
					curImg.style.backgroundImage=e.currentTarget.children[0].children[0].style.backgroundImage;
					curName.innerHTML=e.currentTarget.children[0].children[1].innerHTML;
					audio.src=e.currentTarget.children[0].children[2].innerHTML;
					if(pl.className=="fas fa-pause-circle"){
						pl.className="fas fa-play-circle";
					}
					clearTimeout(timer);
					counter=0;
					audio.curruntTime=0;
					grow.style.width=0;
	};
					list.appendChild(lister);
				}
				}
			break;
		case "By group":
			for(var j=0;j<music.length;j++, qw=0){
				for(var k=0;k<srch.value.length & k<music[j].name.length;k++)
					if(srch.value[k].toLowerCase()==music[j].group[k].toLowerCase()) qw+=1;
				if (qw>srch.value.length-1) {
					lister=Cont.cloneNode(true);
					lister.children[0].children[0].style.backgroundImage="url("+music[j].img+")";
					lister.children[0].children[1].innerHTML=music[j].name+" - "+music[j].group;
					lister.children[0].children[2].innerHTML=music[j].src;
					lister.children[1].children[1].innerHTML=music[j].id;
					lister.children[1].onclick=function(e){
						userInf[0].musId[userInf[0].musId.length]=e.currentTarget.lastElementChild.innerHTML;
					};
					lister.onclick=lister.onclick=function (e){
					curImg.style.backgroundImage=e.currentTarget.children[0].children[0].style.backgroundImage;
					curName.innerHTML=e.currentTarget.children[0].children[1].innerHTML;
					audio.src=e.currentTarget.children[0].children[2].innerHTML;
					if(pl.className=="fas fa-pause-circle"){
						pl.className="fas fa-play-circle";
					}
					clearTimeout(timer);
					counter=0;
					audio.curruntTime=0;
					grow.style.width=0;
	};
					list.appendChild(lister);
				}
				}
			break;
		case "By genre":{
			if(srch.value.match(/[ -_+*&^%$#@!.</?\|]/)) var Value=srch.value.replace(/[ -_+*&^%$#@!.</?\|]/,"");
			else var Value=srch.value;
			for(var j=0;j<musTag.length;j++, qw=0){
				if(Object.keys(musTag[j])[0].match(/[ -_+*&^%$#@!.</?\|]/)) var compare=Object.keys(musTag[j])[0].replace(/[ -_+*&^%$#@!.</?\|]/,"");
				else var compare=Object.keys(musTag[j])[0];
				for(var k=0;k<Value.length && k<compare.length;k++)
					if(Value[k].toLowerCase()==compare[k].toLowerCase()) qw+=1;
				for(var l=0;(qw>Value.length-1)&(l<musTag[j][Object.keys(musTag[j])].length);l++) {
					lister=Cont.cloneNode(true);
					lister.children[0].children[0].style.backgroundImage="url("+musTag[j][Object.keys(musTag[j])][l].img+")";
					lister.children[0].children[1].innerHTML=musTag[j][Object.keys(musTag[j])][l].name+" - "+musTag[j][Object.keys(musTag[j])][l].group;
					lister.children[0].children[2].innerHTML=musTag[j][Object.keys(musTag[j])][l].src;
					lister.children[1].children[1].innerHTML=music[j].id;
					lister.children[1].onclick=function(e){
						userInf[0].musId[userInf[0].musId.length]=e.currentTarget.lastElementChild.innerHTML;
					};
					lister.onclick=lister.onclick=function (e){
					curImg.style.backgroundImage=e.currentTarget.children[0].children[0].style.backgroundImage;
					curName.innerHTML=e.currentTarget.children[0].children[1].innerHTML;
					audio.src=e.currentTarget.children[0].children[2].innerHTML;
					if(pl.className=="fas fa-pause-circle"){
						pl.className="fas fa-play-circle";
					}
					clearTimeout(timer);
					counter=0;
					audio.curruntTime=0;
					grow.style.width=0;
	};
					list.appendChild(lister);
				}
				}
			break;
		}
	}}
	else load();};
function schClear(){
	srch.value="";
};

var climax=document.getElementsByClassName("clim")[0];

climax.addEventListener("click",function(el){
	if(climax.style.color=="rgb(0, 0, 0)"){
		climax.style.color="rgb(160, 173, 173)";
		climax.nextElementSibling.style.color="rgb(160, 173, 173)";
	}
	else{ climax.style.color="rgb(0, 0, 0)";
		climax.nextElementSibling.style.color="rgb(0,0,0)";
	}
	climax.nextElementSibling.firstElementChild.addEventListener("click",function(e){
	if(e.target.innerHTML<2)
		e.target.innerHTML='<input type="text" class="climax-inp" style="width:8px;" value='+e.target.innerHTML+'>';
});
	climax.nextElementSibling.lastElementChild.addEventListener("click",function(e){
	if(e.target.innerHTML<3)
		e.target.innerHTML='<input type="text" class="climax-inp" style="width:16px;" value='+e.target.innerHTML+'>';
});
});
var duration=document.getElementsByClassName("durn")[0];
	duration.nextElementSibling.firstElementChild.addEventListener("click",function(e){
	if(e.target.innerHTML<2)
		e.target.innerHTML='<input type="text" class="climax-inp" style="width:8px;" value='+e.target.innerHTML+'>';
});
	duration.nextElementSibling.lastElementChild.addEventListener("click",function(e){
	if(e.target.innerHTML<3)
		e.target.innerHTML='<input type="text" class="climax-inp" style="width:16px;" value='+e.target.innerHTML+'>';
});



