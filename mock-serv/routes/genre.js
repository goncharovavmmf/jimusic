const express = require('express');
const router = express.Router();
var faker = require('faker');
/* GET users listing. res.writehead(200 'content-type' 'text/plain' )*/
router.get('/', (req, res) => {
  res.send([{
		"name": faker.lorem.word(),
		"id": "1"
	},
	{
		"name": faker.lorem.word(),
		"id": "2"
	},
	{
		"name": faker.lorem.word(),
		"id": "3"
	},
	{
		"name": faker.lorem.word(),
		"id": "4"
	}
]);
});
router.get('/:genreId', (req, res) => {
  res.send({
		"name": faker.lorem.word(),
		"id": req.params.genreId
	});
});
module.exports = router;
