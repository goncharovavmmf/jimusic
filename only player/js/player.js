var audio=document.getElementsByClassName("aud")[0];
var pl=document.getElementsByClassName("play")[0].firstElementChild;
var grow=document.getElementsByClassName("grow")[0];
var tim=document.getElementsByClassName("timer")[0];
var vol=document.getElementsByClassName("vol-range")[0];
var timer;
var dur=0;
var minutes=0;
var seconds=0;
var ratio;
//var genr=document.getElementsByClassName("genre")[0];
audio.onloadedmetadata = function() {
dur=parseInt(audio.duration);
minutes = parseInt(audio.duration / 60, 10);
seconds = parseInt(audio.duration % 60);
ratio = 300/audio.duration;
};
var i=0;
var j=0;
var counter=0;
function play(){
	if(pl.className=="fas fa-pause-circle"){
	audio.pause();
	pl.className="fas fa-play-circle";
	clearTimeout(timer);
	}
	else {
	audio.play();
	pl.className="fas fa-pause-circle";
	timer=setTimeout(function recursion(){
		min = parseInt(audio.currentTime / 60, 10);
		sec = parseInt(audio.currentTime % 60);
		counter++;
		if (seconds>sec)
			if((seconds-sec)<10)
				tim.innerHTML=(minutes-min)+":0"+(seconds-sec);
			else tim.innerHTML=(minutes-min)+":"+(seconds-sec);
		else
			tim.innerHTML=(minutes-min-1)+":"+(seconds-sec+59);
		if (audio.duration-audio.currentTime){ 
				grow.style.width=counter*ratio+'px';
				timer=setTimeout(recursion,1000);
		}
		else{ tim.innerHTML="0:00";counter=0; grow.style.width=0+'px'; play();}
},1000)
	}
}
	var warp=document.getElementsByClassName("vol-warp")[0];
    var wid=warp.offsetWidth;
	var gr=document.getElementsByClassName("grow-y")[0];
var thumbElem = warp.children[0].children[1];
    thumbElem.onmousedown = function(e) {
      var thumbCoords = getCoords(thumbElem);
      var shiftX = e.pageX - thumbCoords.left;
      // shiftY здесь не нужен, слайдер двигается только по горизонтали

      var sliderCoords = getCoords(warp);

      document.onmousemove = function(e) {
        //  вычесть координату родителя, т.к. position: relative
        var newLeft = e.pageX - shiftX - sliderCoords.left;

        // курсор ушёл вне слайдера
        if (newLeft < -(warp.offsetWidth - thumbElem.offsetWidth)/2) {
          newLeft = -(warp.offsetWidth - thumbElem.offsetWidth)/2;
        }
        var rightEdge = (warp.offsetWidth - thumbElem.offsetWidth);
        if (newLeft > rightEdge) {
          newLeft = rightEdge;
        }

		gr.style.width=newLeft+'px';
		if(newLeft>1)audio.volume=newLeft/44;
		else audio.volume=0;
		
      }
      document.onmouseup = function() {
        document.onmousemove = document.onmouseup = null;
      };

      return false; // disable selection start (cursor change)
    };

    thumbElem.ondragstart = function() {
      return false;
    };

    function getCoords(elem) { // кроме IE8-
      var box = elem.getBoundingClientRect();
      return {
		 top: box.top + pageYOffset,
        left: box.left + pageXOffset
      };

    }
    


var k;
/*function genre(){
		var div1=document.createElement('div');
		genr.appendChild(div1);
		div1.className="to-center";
	for(k=0;k<gen.length;k++){
		var div=document.createElement('div');
		var i=document.createElement('i');
		div1.appendChild(div);
		div.className="add";
		div.innerHTML=gen[k];
		div.appendChild(i);
		div.style.width="80%";
		i.className="fas fa-plus";
		i.onclick=function addGenre(event){
	var res=document.getElementsByClassName("reslt-genre")[0];
	var cl=event.target.parentElement.cloneNode(true);
	cl.lastElementChild.className="fas fa-minus";
	cl.lastElementChild.onclick=function remove(event){
		event.target.parentElement.remove();
	}
	if (res.childNodes[1]) res.replaceChild(cl,res.lastElementChild);
	else res.appendChild(cl);
	
}
	}
}
function mood(){
	var moo=document.getElementsByClassName("mood")[0];
	var div1=document.createElement('div');
	moo.appendChild(div1);
	div1.className="to-center";
for(var k=0;k<md.length;k++){
	var div=document.createElement('div');
	var i=document.createElement('i');
	div1.appendChild(div);
	div.className="add";
	div.innerHTML=md[k];
	div.appendChild(i);
	div.style.width="80%";
	i.className="fas fa-plus";
	i.onclick=function addMood(event){
	var res=document.getElementsByClassName("reslt-mood")[0];
	var cl=event.target.parentElement.cloneNode(true);
	cl.lastElementChild.className="fas fa-minus";
	cl.lastElementChild.onclick=function remove(event){
		event.target.parentElement.remove();
	}
	if (res.childNodes[1]) res.replaceChild(cl,res.lastElementChild);
	else res.appendChild(cl);
	
}
}
}*/
var mot=document.getElementsByClassName("road-warp")[0];
mot.addEventListener("click",function(e){
if(pl.className=="fas fa-play-circle") play();
if (e.target.className!="motion"){
	var x = e.offsetX;
	play();
	counter=x/ratio;
	play();
	grow.style.width=x+'px';
	audio.currentTime=parseInt(x/300*dur);
			
}
});
mot.addEventListener("mousemove",function (e){
var show=document.getElementsByClassName("sh-time")[0];
show.style.left=e.clientX-show.offsetWidth/2+'px';
if(parseInt(e.offsetX/ratio %60)>9)
	show.innerHTML=parseInt(e.offsetX/ratio/60)+":"+parseInt(e.offsetX/ratio %60);
else 	show.innerHTML=parseInt(e.offsetX/ratio/60)+":0"+parseInt(e.offsetX/ratio %60);
show.style.display="block";
mot.onmouseout=function(){show.style.display="none";};
});
var list=document.getElementsByClassName("mus-list")[0];
var curImg=document.getElementsByClassName("act-img")[0];
var curName=document.getElementsByClassName("music-name")[0];
var Cont=document.createElement('div');
Cont.className="cont";
var musCont=document.createElement('div');
musCont.className="lister";
var imgCont=document.createElement('div');
imgCont.className="poster-listed";
var textCont=document.createElement('div');
textCont.className="text-listed";
var audioCont=document.createElement('audio');
Cont.appendChild(musCont);
musCont.appendChild(imgCont);
musCont.appendChild(textCont);
musCont.appendChild(audioCont);
function load(){ for(var p=0;p<music.length;p++){
	var lister=Cont.cloneNode(true);
	lister.children[0].children[0].style.backgroundImage="url("+music[p].img+")";
	lister.children[0].children[1].innerHTML=music[p].name+" - "+music[p].group;
	lister.children[0].children[2].innerHTML=music[p].src;
	list.appendChild(lister);
lister.onclick=function (e){
curImg.style.backgroundImage=e.currentTarget.children[0].children[0].style.backgroundImage;
curName.innerHTML=e.currentTarget.children[0].children[1].innerHTML;
audio.src=e.currentTarget.children[0].children[2].innerHTML;
if(pl.className=="fas fa-pause-circle"){
	pl.className="fas fa-play-circle";
}
clearTimeout(timer);
counter=0;
audio.curruntTime=0;
grow.style.width=0;
console.log();
};
};
};

var srch=document.getElementsByClassName("srch")[0];
var srchBtn=document.getElementsByClassName("search-btn")[0];
srch.addEventListener("keypress", function(event) {
    if (event.keyCode == 13)
        srchBtn.click();
});
function Search(){
	if(srch.value.length){
while (list.firstChild) {
    list.removeChild(list.firstChild);
}var qw=0;
var qwo=0;
	for(var j=0;j<music.length;j++){
		var id=music[j].id;
		for(var k=0;k<srch.value.length;k++){
		id=music[j].id;
		if(srch.value[k].toLowerCase()==music[j].name[k].toLowerCase()) qw+=1;
		if(srch.value[k].toLowerCase()==music[j].group[k].toLowerCase()) qwo+=1;
		}
				console.log("qw"+qw+" "+j);
	if (qw>=srch.value.length-1) {
		lister=Cont.cloneNode(true);
		lister.children[0].children[0].style.backgroundImage="url("+music[j].img+")";
		lister.children[0].children[1].innerHTML=music[j].name+" - "+music[j].group;
		lister.children[0].children[2].innerHTML=music[j].src;
		list.appendChild(lister);
		qwo=0;
		lister.onclick=lister.onclick=function (e){
curImg.style.backgroundImage=e.currentTarget.children[0].children[0].style.backgroundImage;
curName.innerHTML=e.currentTarget.children[0].children[1].innerHTML;
audio.src=e.currentTarget.children[0].children[2].innerHTML;
if(pl.className=="fas fa-pause-circle"){
	pl.className="fas fa-play-circle";
}
clearTimeout(timer);
counter=0;
audio.curruntTime=0;
grow.style.width=0;
console.log();
};
		}
		if (qwo>srch.value.length-1) {
		lister=Cont.cloneNode(true);
		lister.children[0].children[0].style.backgroundImage="url("+music[j].img+")";
		lister.children[0].children[1].innerHTML=music[j].name+" - "+music[j].group;
		lister.children[0].children[2].innerHTML=music[j].src;
		list.appendChild(lister);
		lister.onclick=lister.onclick=function (e){
curImg.style.backgroundImage=e.currentTarget.children[0].children[0].style.backgroundImage;
curName.innerHTML=e.currentTarget.children[0].children[1].innerHTML;
audio.src=e.currentTarget.children[0].children[2].innerHTML;
if(pl.className=="fas fa-pause-circle"){
	pl.className="fas fa-play-circle";
}
clearTimeout(timer);
counter=0;
audio.curruntTime=0;
grow.style.width=0;
console.log();
};
		}
	qw=0;
	qwo=0;
};}
	else {while (list.firstChild) {
list.removeChild(list.firstChild)}; load();}};
function schClear(){
	srch.value="";
};